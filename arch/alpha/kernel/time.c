/*
 *  linux/arch/alpha/kernel/time.c
 *
 *  Copyright (C) 1991, 1992, 1995, 1999, 2000  Linus Torvalds
 *
 * This file contains the PC-specific time handling details:
 * reading the RTC at bootup, etc..
 * 1994-07-02    Alan Modra
 *	fixed set_rtc_mmss, fixed time.year for >= 2000, new mktime
 * 1995-03-26    Markus Kuhn
 *      fixed 500 ms bug at call to set_rtc_mmss, fixed DS12887
 *      precision CMOS clock update
 * 1997-09-10	Updated NTP code according to technical memorandum Jan '96
 *		"A Kernel Model for Precision Timekeeping" by Dave Mills
 * 1997-01-09    Adrian Sun
 *      use interval timer if CONFIG_RTC=y
 * 1997-10-29    John Bowman (bowman@math.ualberta.ca)
 *      fixed tick loss calculation in timer_interrupt
 *      (round system clock to nearest tick instead of truncating)
 *      fixed algorithm in time_init for getting time from CMOS clock
 * 1999-04-16	Thorsten Kranzkowski (dl8bcu@gmx.net)
 *	fixed algorithm in do_gettimeofday() for calculating the precise time
 *	from processor cycle counter (now taking lost_ticks into account)
 * 2000-08-13	Jan-Benedict Glaw <jbglaw@lug-owl.de>
 * 	Fixed time_init to be aware of epoches != 1900. This prevents
 * 	booting up in 2048 for me;) Code is stolen from rtc.c.
 * 2003-06-03	R. Scott Bailey <scott.bailey@eds.com>
 *	Tighten sanity in time_init from 1% (10,000 PPM) to 250 PPM
 */
#include <linux/errno.h>
#include <linux/module.h>
#include <linux/sched.h>
#include <linux/kernel.h>
#include <linux/param.h>
#include <linux/string.h>
#include <linux/mm.h>
#include <linux/delay.h>
#include <linux/ioport.h>
#include <linux/irq.h>
#include <linux/interrupt.h>
#include <linux/init.h>
#include <linux/bcd.h>
#include <linux/profile.h>
#include <linux/irq_work.h>
#include <linux/clocksource.h>
#include <linux/clockchips.h>

#include <asm/uaccess.h>
#include <asm/io.h>
#include <asm/hwrpb.h>
#include <asm/8253pit.h>
#include <asm/rtc.h>

#include <linux/mc146818rtc.h>
#include <linux/time.h>
#include <linux/timex.h>
#include <linux/clocksource.h>

#include "proto.h"
#include "irq_impl.h"

DEFINE_SPINLOCK(rtc_lock);
EXPORT_SYMBOL(rtc_lock);

/* Before time_init, this is the cycle frequency as set by the command line.
   After time_init, this is the cycle frequency that we've used in setting
   up the rpcc clocksource.  */
unsigned long est_cycle_freq;


/*
 * The RPCC as a cyclecounter primitive.
 */

static cycle_t
rpcc_cc_read(const struct cyclecounter *cc)
{
	return __builtin_alpha_rpcc();
}

static struct cyclecounter rpcc_cc = {
	.read	= rpcc_cc_read,
	.mask	= CLOCKSOURCE_MASK(32),
};


/*
 * The combination of RTC + RPCC as a clock_event_device primitive.
 *
 * Here we get to cooperate with cpu_idle to skip sequences of RTC
 * clock ticks in order to put the cpu into a low-power mode.
 */

struct rtc_rpcc_info 
{
	struct clock_event_device ce;
	struct timecounter tc;
	unsigned long ce_oneshot;
	long max_skip_ticks;
};

static DEFINE_PER_CPU(struct rtc_rpcc_info, rtc_rpcc_info);

static irqreturn_t
rtc_timer_interrupt(void)
{
	int cpu = smp_processor_id();
	struct rtc_rpcc_info *info = &per_cpu(rtc_rpcc_info, cpu);
	long delta;
	u64 now;

	/* Always read the timecounter.  This has the side-effect of
	   accounting for overflow in RPCC.  We want to ensure that this
	   is called often enough (say, ever 4-5 seconds) so that we
	   never miss an overflow.  */
	now = timecounter_read(&info->tc);

	/* Determine if this clock_event_device was supposed to fire.  */
	switch (info->ce.mode) {
	case CLOCK_EVT_MODE_PERIODIC:
		/* In periodic mode, we deliver every event.  */
		break;

	case CLOCK_EVT_MODE_ONESHOT:
		/* If we've already done the event, don't do it again.  */
		if (info->ce_oneshot == 0)
			return IRQ_HANDLED;

		/* If the time for the event is farther away than 1/2 HZ,
		   do not deliver the event at this time.  */
		delta = info->ce_oneshot - now;
		delta -= info->ce.min_delta_ns / 2;
		if (delta > 0)
			return IRQ_HANDLED;

		/* Indicate that we've done our duty.  */
		info->ce_oneshot = 0;
		break;

	default:
		/* If we're shutdown, there's nothing to do.  */
		WARN_ON(info->ce.mode != CLOCK_EVT_MODE_SHUTDOWN);
		return IRQ_HANDLED;
	}

	info->ce.event_handler(&info->ce);
	return IRQ_HANDLED;
}

static void
rtc_ce_set_mode(enum clock_event_mode mode, struct clock_event_device *x_ce)
{
	struct rtc_rpcc_info *info = (struct rtc_rpcc_info *)
	  ((long)x_ce - offsetof(struct rtc_rpcc_info, ce));

	/* The mode member of X_CE is updated for us in generic code.
	   Given that we never actually change the hardware interrupt
	   source, simply make sure that ONESHOT is disabled.  */
	info->ce_oneshot = 0;
}

static int
rtc_ce_set_next_event(unsigned long evt, struct clock_event_device *x_ce)
{
	struct rtc_rpcc_info *info = (struct rtc_rpcc_info *)
	  ((long)x_ce - offsetof(struct rtc_rpcc_info, ce));

	BUG_ON(info->ce.mode != CLOCK_EVT_MODE_ONESHOT);

	info->ce_oneshot = timecounter_read(&info->tc) + evt;
	return 0;
}

static long
choose_skip_ticks(struct rtc_rpcc_info *info)
{
	long delta, skip_ticks = info->max_skip_ticks;

	if (alpha_using_qemu)
		return 0;

	switch (info->ce.mode) {
	case CLOCK_EVT_MODE_PERIODIC:
		return 0;

	case CLOCK_EVT_MODE_ONESHOT:
		delta = info->ce_oneshot;
		if (delta == 0)
			break;
		delta -= timecounter_read(&info->tc);
		if (delta <= info->ce.min_delta_ns)
			return 0;
		if (delta < info->ce.max_delta_ns)
			skip_ticks = delta / (NSEC_PER_SEC / HZ);
		break;

	default:
		/* If we're shutdown, there's nothing to do.  */
		WARN_ON(info->ce.mode != CLOCK_EVT_MODE_SHUTDOWN);
		break;
	}

	return skip_ticks;
}

void
cpu_idle(void)
{
	int cpu = smp_processor_id();
	struct rtc_rpcc_info *info = &per_cpu(rtc_rpcc_info, cpu);

	set_thread_flag(TIF_POLLING_NRFLAG);

	while (1) {
		while (!need_resched())
			wtint(choose_skip_ticks(info));
		schedule();
	}
}

void __init
init_rtc_rpcc_info(void)
{
	int cpu = smp_processor_id();
	struct rtc_rpcc_info *info = &per_cpu(rtc_rpcc_info, cpu);

	info->ce.name = "rtc";
	info->ce.features = CLOCK_EVT_FEAT_PERIODIC | CLOCK_EVT_FEAT_ONESHOT;
	info->ce.min_delta_ns = NSEC_PER_SEC / HZ;
	info->ce.rating = 100;
	info->ce.cpumask = cpumask_of(cpu);
	info->ce.set_next_event = rtc_ce_set_next_event;
	info->ce.set_mode = rtc_ce_set_mode;

	/* We use the periodic clock tick to fixup overflow in RPCC.
	   Therefore we do not want to skip too many events.  Let the
	   maximum be 1/3 of the time between overflows.  */
	info->ce.max_delta_ns
	  = ((u64)NSEC_PER_SEC << 32) / (est_cycle_freq * 3);

	/* And the same info in units of clock ticks, i.e. HZ.  */
	info->max_skip_ticks = (info->ce.max_delta_ns * HZ) / NSEC_PER_SEC;

	/* Yes, the clock actually runs at HZ, but we're going to compare
	   times against timecounter_read results, therefore we choose to
	   tell the generic code that we count NSEC.  */
	info->ce.mult = 1;
	info->ce.shift = 0;

	clockevents_register_device(&info->ce);

	timecounter_init(&info->tc, &rpcc_cc, ktime_to_ns(ktime_get()));
}


/*
 * The RPCC as a clocksource primitive.
 *
 * While we have free-running timecounters running on all CPUs, and we make
 * a half-hearted attempt in init_rtc_rpcc_info to sync the timecounter
 * with the wall clock, that initialization isn't kept up-to-date across
 * different time counters in SMP mode.  Therefore we can only use this
 * method when there's only one CPU enabled.
 */

static cycle_t
read_rpcc_tc(struct clocksource *cs)
{
	struct rtc_rpcc_info *info = &per_cpu(rtc_rpcc_info, boot_cpuid);
	return timecounter_read(&info->tc);
}

static struct clocksource rpcc_cs = {
	.name                   = "rpcc",
	.rating                 = 300,
	.read                   = read_rpcc_tc,
	.mask                   = CLOCKSOURCE_MASK(64),
	.flags                  = CLOCK_SOURCE_IS_CONTINUOUS,
	/* Note that the timecounter on which we are based has already
	   done the conversion to NSEC.  No need to do it twice.  */
	.mult			= 1,
	.shift			= 0,
};


/*
 * The QEMU clock as a clocksource primitive.
 */

static inline unsigned long
qemu_get_time(void)
{
	register unsigned long v0 __asm__("$0");
	register unsigned long a0 __asm__("$16") = 3;

	asm("call_pal %2 # cserve get_time"
	    : "=r"(v0), "+r"(a0)
	    : "i"(PAL_cserve)
	    : "$17", "$18", "$19", "$20", "$21");

	return v0;
}

static cycle_t
qemu_cs_read(struct clocksource *cs)
{
	return qemu_get_time();
}

static struct clocksource qemu_cs = {
	.name                   = "qemu",
	.rating                 = 400,
	.read                   = qemu_cs_read,
	.mask                   = CLOCKSOURCE_MASK(64),
	.flags                  = CLOCK_SOURCE_IS_CONTINUOUS,
	.mult			= 1,
	.shift			= 0,
	.max_idle_ns		= LONG_MAX
};

/*
 * The QEMU clock and alarm as a clock_event_device primitive.
 */

static inline unsigned long
qemu_get_alarm(void)
{
	register unsigned long v0 __asm__("$0");
	register unsigned long a0 __asm__("$16") = 4;

	asm("call_pal %2 # cserve get_alarm"
	    : "=r"(v0), "+r"(a0)
	    : "i"(PAL_cserve)
	    : "$17", "$18", "$19", "$20", "$21");

	return v0;
}

static inline void
qemu_set_alarm_rel(unsigned long expire)
{
	register unsigned long a0 __asm__("$16") = 5;
	register unsigned long a1 __asm__("$17") = expire;

	asm volatile("call_pal %2 # cserve set_alarm_rel"
		     : "+r"(a0), "+r"(a1)
		     : "i"(PAL_cserve)
		     : "$0", "$18", "$19", "$20", "$21");
}

static inline void
qemu_set_alarm_abs(unsigned long expire)
{
	register unsigned long a0 __asm__("$16") = 6;
	register unsigned long a1 __asm__("$17") = expire;

	asm volatile("call_pal %2 # cserve set_alarm_abs"
		     : "+r"(a0), "+r"(a1)
		     : "i"(PAL_cserve)
		     : "$0", "$18", "$19", "$20", "$21");
}

static void
qemu_ce_set_mode(enum clock_event_mode mode, struct clock_event_device *ce)
{
	/* The mode member of CE is updated for us in generic code.
	   Just make sure that the event is disabled.  */
	qemu_set_alarm_abs(0);
}

static int
qemu_ce_set_next_event(unsigned long evt, struct clock_event_device *ce)
{
	qemu_set_alarm_rel(evt);
	return 0;
}

static irqreturn_t
qemu_timer_interrupt(void)
{
	int cpu = smp_processor_id();
	struct rtc_rpcc_info *info = &per_cpu(rtc_rpcc_info, cpu);

	info->ce.event_handler(&info->ce);
	return IRQ_HANDLED;
}

void __init
init_qemu_clock(void)
{
	/* Re-use the already pre-allocated clock event structure.  */
	int cpu = smp_processor_id();
	struct rtc_rpcc_info *info = &per_cpu(rtc_rpcc_info, cpu);
	struct clock_event_device *ce = &info->ce;

	ce->name = "qemu";
	ce->features = CLOCK_EVT_FEAT_ONESHOT;
	ce->min_delta_ns = 1000;
	ce->max_delta_ns = LONG_MAX;
	ce->mult = 1;
	ce->shift = 0;
	ce->rating = 400;
	ce->cpumask = cpumask_of(cpu);
	ce->set_next_event = qemu_ce_set_next_event;
	ce->set_mode = qemu_ce_set_mode;

	clockevents_register_device(ce);
	clocksource_register(&qemu_cs);
}

/* Validate a computed cycle counter result against the known bounds for
   the given processor core.  There's too much brokenness in the way of
   timing hardware for any one method to work everywhere.  :-(

   Return 0 if the result cannot be trusted, otherwise return the argument.  */

static unsigned long __init
validate_cc_value(unsigned long cc)
{
	static struct bounds {
		unsigned int min, max;
	} cpu_hz[] __initdata = {
		[EV3_CPU]    = {   50000000,  200000000 },	/* guess */
		[EV4_CPU]    = {  100000000,  300000000 },
		[LCA4_CPU]   = {  100000000,  300000000 },	/* guess */
		[EV45_CPU]   = {  200000000,  300000000 },
		[EV5_CPU]    = {  250000000,  433000000 },
		[EV56_CPU]   = {  333000000,  667000000 },
		[PCA56_CPU]  = {  400000000,  600000000 },	/* guess */
		[PCA57_CPU]  = {  500000000,  600000000 },	/* guess */
		[EV6_CPU]    = {  466000000,  600000000 },
		[EV67_CPU]   = {  600000000,  750000000 },
		[EV68AL_CPU] = {  750000000,  940000000 },
		[EV68CB_CPU] = { 1000000000, 1333333333 },
		/* None of the following are shipping as of 2001-11-01.  */
		[EV68CX_CPU] = { 1000000000, 1700000000 },	/* guess */
		[EV69_CPU]   = { 1000000000, 1700000000 },	/* guess */
		[EV7_CPU]    = {  800000000, 1400000000 },	/* guess */
		[EV79_CPU]   = { 1000000000, 2000000000 },	/* guess */
	};

	/* Allow for some drift in the crystal.  10MHz is more than enough.  */
	const unsigned int deviation = 10000000;

	struct percpu_struct *cpu;
	unsigned int index;

	cpu = (struct percpu_struct *)((char*)hwrpb + hwrpb->processor_offset);
	index = cpu->type & 0xffffffff;

	/* If index out of bounds, no way to validate.  */
	if (index >= ARRAY_SIZE(cpu_hz))
		return cc;

	/* If index contains no data, no way to validate.  */
	if (cpu_hz[index].max == 0)
		return cc;

	if (cc < cpu_hz[index].min - deviation
	    || cc > cpu_hz[index].max + deviation)
		return 0;

	return cc;
}


/*
 * Calibrate CPU clock using legacy 8254 timer/counter. Stolen from
 * arch/i386/time.c.
 */

#define CALIBRATE_LATCH	0xffff
#define TIMEOUT_COUNT	0x100000

static unsigned long __init
calibrate_cc_with_pit(void)
{
	int cc, count = 0;

	/* Set the Gate high, disable speaker */
	outb((inb(0x61) & ~0x02) | 0x01, 0x61);

	/*
	 * Now let's take care of CTC channel 2
	 *
	 * Set the Gate high, program CTC channel 2 for mode 0,
	 * (interrupt on terminal count mode), binary count,
	 * load 5 * LATCH count, (LSB and MSB) to begin countdown.
	 */
	outb(0xb0, 0x43);		/* binary, mode 0, LSB/MSB, Ch 2 */
	outb(CALIBRATE_LATCH & 0xff, 0x42);	/* LSB of count */
	outb(CALIBRATE_LATCH >> 8, 0x42);	/* MSB of count */

	cc = __builtin_alpha_rpcc();
	do {
		count++;
	} while ((inb(0x61) & 0x20) == 0 && count < TIMEOUT_COUNT);
	cc = __builtin_alpha_rpcc() - cc;

	/* Error: ECTCNEVERSET or ECPUTOOFAST.  */
	if (count <= 1 || count == TIMEOUT_COUNT)
		return 0;

	return ((long)cc * PIT_TICK_RATE) / (CALIBRATE_LATCH + 1);
}

/* The Linux interpretation of the CMOS clock register contents:
   When the Update-In-Progress (UIP) flag goes from 1 to 0, the
   RTC registers show the second which has precisely just started.
   Let's hope other operating systems interpret the RTC the same way.  */

static unsigned long __init
rpcc_after_update_in_progress(void)
{
	do { } while (!(CMOS_READ(RTC_FREQ_SELECT) & RTC_UIP));
	do { } while (CMOS_READ(RTC_FREQ_SELECT) & RTC_UIP);

	return __builtin_alpha_rpcc();
}

void __init
time_init(void)
{
	unsigned int cc1, cc2;
	unsigned long cycle_freq, tolerance;
	long diff;

	if (alpha_using_qemu) {
		init_rtc_irq();
		init_qemu_clock();
		return;
	}

	/* Calibrate CPU clock -- attempt #1.  */
	if (!est_cycle_freq)
		est_cycle_freq = validate_cc_value(calibrate_cc_with_pit());

	cc1 = __builtin_alpha_rpcc();

	/* Calibrate CPU clock -- attempt #2.  */
	if (!est_cycle_freq) {
		cc1 = rpcc_after_update_in_progress();
		cc2 = rpcc_after_update_in_progress();
		est_cycle_freq = validate_cc_value(cc2 - cc1);
		cc1 = cc2;
	}

	cycle_freq = hwrpb->cycle_freq;
	if (est_cycle_freq) {
		/* If the given value is within 250 PPM of what we calculated,
		   accept it.  Otherwise, use what we found.  */
		tolerance = cycle_freq / 4000;
		diff = cycle_freq - est_cycle_freq;
		if (diff < 0)
			diff = -diff;
		if ((unsigned long)diff > tolerance) {
			cycle_freq = est_cycle_freq;
			printk("HWRPB cycle frequency bogus.  "
			       "Estimated %lu Hz\n", cycle_freq);
		} else {
			est_cycle_freq = 0;
		}
	} else if (! validate_cc_value (cycle_freq)) {
		printk("HWRPB cycle frequency bogus, "
		       "and unable to estimate a proper value!\n");
	}
	est_cycle_freq = cycle_freq;

	/* Always configure the RPCC as a free-running cycle-counter.  */
	clocks_calc_mult_shift(&rpcc_cc.mult, &rpcc_cc.shift, cycle_freq,
			       NSEC_PER_SEC, 4);

	if (hwrpb->nr_processors == 1)
		clocksource_register(&rpcc_cs);

	/* From John Bowman <bowman@math.ualberta.ca>: allow the values
	   to settle, as the Update-In-Progress bit going low isn't good
	   enough on some hardware.  2ms is our guess; we haven't found 
	   bogomips yet, but this is close on a 500Mhz box.  */
	__delay(1000000);

	/* Startup the timer source. */
	alpha_mv.init_rtc();

	/* Startup the clock_event_device on the boot cpu.  */
	init_rtc_rpcc_info();
}

irqreturn_t
timer_interrupt(int irq, void *dev)
{
	if (alpha_using_qemu)
		return qemu_timer_interrupt();
	else
		return rtc_timer_interrupt();
}

/*
 * The RTC as a persistent clock.
 */

void __init
common_init_rtc(void)
{
	unsigned char x;

	/* Reset periodic interrupt frequency.  */
	x = CMOS_READ(RTC_FREQ_SELECT) & 0x3f;
        /* Test includes known working values on various platforms
           where 0x26 is wrong; we refuse to change those. */
	if (x != 0x26 && x != 0x25 && x != 0x19 && x != 0x06) {
		printk("Setting RTC_FREQ to 1024 Hz (%x)\n", x);
		CMOS_WRITE(0x26, RTC_FREQ_SELECT);
	}

	/* Turn on periodic interrupts.  */
	x = CMOS_READ(RTC_CONTROL);
	if (!(x & RTC_PIE)) {
		printk("Turning on RTC interrupts.\n");
		x |= RTC_PIE;
		x &= ~(RTC_AIE | RTC_UIE);
		CMOS_WRITE(x, RTC_CONTROL);
	}
	(void) CMOS_READ(RTC_INTR_FLAGS);

	outb(0x36, 0x43);	/* pit counter 0: system timer */
	outb(0x00, 0x40);
	outb(0x00, 0x40);

	outb(0xb6, 0x43);	/* pit counter 2: speaker */
	outb(0x31, 0x42);
	outb(0x13, 0x42);

	init_rtc_irq();
}

unsigned int common_get_rtc_time(struct rtc_time *time)
{
	return __get_rtc_time(time);
}

int common_set_rtc_time(struct rtc_time *time)
{
	return __set_rtc_time(time);
}

int update_persistent_clock(struct timespec now)
{
	struct rtc_time tm;

	if (alpha_using_qemu)
		return -1;

	rtc_time_to_tm(now.tv_sec, &tm);
	return set_rtc_time(&tm);
}

void read_persistent_clock(struct timespec *ts)
{
	struct rtc_time tm;

	if (alpha_using_qemu) {
		unsigned long wall_ns = qemu_get_time();
		ts->tv_sec = wall_ns / NSEC_PER_SEC;
		ts->tv_nsec = wall_ns - (ts->tv_sec * NSEC_PER_SEC);
		return;
	}

	ts->tv_nsec = 0;
	get_rtc_time(&tm);
	rtc_tm_to_time(&tm, &ts->tv_sec);
}
