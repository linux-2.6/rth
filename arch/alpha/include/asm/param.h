#ifndef _ASM_ALPHA_PARAM_H
#define _ASM_ALPHA_PARAM_H

#ifdef __KERNEL__
/* Internal kernel timer frequency.  */
#define HZ		CONFIG_HZ

/* Some user interfaces are in "ticks", like times(2).  */
#define USER_HZ	1024
#define CLOCKS_PER_SEC	USER_HZ
#endif

#ifndef HZ
#define HZ		1024
#endif

#define EXEC_PAGESIZE	8192

#ifndef NOGROUP
#define NOGROUP		(-1)
#endif

#define MAXHOSTNAMELEN	64	/* max length of hostname */

#endif /* _ASM_ALPHA_PARAM_H */
